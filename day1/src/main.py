from pathlib import Path

puzzle_input = Path.home() / 'chas2022/aoc_labb/day1/src/input.txt'
test_input = ["+1", "-2", "-3", "+2", "+5", "+1"]
test_input_problem_2 = ['+7', '+7', '-2', '-7', '-4']


def problem_1(a):
    start = 0
    for i in a:
        if i[0] == '+':
            start += int(i[1:])
        elif i[0] == '-':
            start -= int(i[1:])
    return start


def problem_2(a):
    start = 0
    result_list = []
    while start not in result_list:
        for i in a:
            if start not in result_list:
                result_list.append(start)
                if i[0] == '+':
                    start += int(i[1:])
                elif i[0] == '-':
                    start -= int(i[1:])
            else:
                return start


if __name__ == '__main__': # pragma: no cover
    puzzle_input_list = []
    with puzzle_input.open(mode='r', encoding='utf-8') as file:
        for i in file.readlines():  # pragma: no cover
            puzzle_input_list.append(i)

        problem_1(puzzle_input_list) # This worked
        problem_2(puzzle_input_list) # This worked
