import pytest
import main

test_input = ["+1", "-2", "-3", "+2", "+5", "+1"]
test_input_problem_2 = ['+7', '+7', '-2', '-7', '-4']


# Tests problem 1 from main.py if test_input (list) is entered
def test_day1_problem_1():
    assert main.problem_1(test_input) == 4


# Tests problem 2 from main.py if test_input_problem_2 (list) is entered
def test_day1_problem_2():
    assert main.problem_2(test_input_problem_2) == 14
