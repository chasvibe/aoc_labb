import pytest
import main_day2

test_input = [
        'abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab'
    ]
test_input_problem_2 = [
        'abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz'
    ]
unformated_strings = ['fghij', 'fguij']


# Tests problem 1 from main.py if test_input (list) is entered
def test_day2_problem_1():
    '''
    The function should take all strings within the test_input list and
    compare them against a pattern to determine an int in return
    '''
    assert main_day2.problem_1(test_input) == 12


def test_day2_problem_2():
    '''
    The function should take all strings within test_input_problem_2 and
    compare to each of the strings in the list to find any similarities
    and return the strings that only differ by exactly ONE letter:
    "fghij" and "fguij" only differ at the middle letter, producing:
    "fgij"
    '''
    assert main_day2.problem_2(test_input_problem_2) == 'fgij'


def test_day2_problem_2_result():
    assert main_day2.result_problem_2(unformated_strings) == 'fgij'
