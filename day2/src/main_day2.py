from pathlib import Path
from difflib import SequenceMatcher, Differ

puzzle_input = Path.home() / 'chas2022/aoc_labb/day2/src/input.txt'
test_input = [
        'abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab'
    ]
test_input_problem_2 = [
        'abcde', 'fghij', 'klmno', 'pqrst', 'fguij', 'axcye', 'wvxyz'
    ]



def problem_1(a):
    twice = 0
    thrice = 0
    for i in a:
        two = False
        three = False
        for x in i:
            if i.count(x) == 2 and not two:
                twice += 1
                two = True
            elif i.count(x) == 3 and not three:
                thrice += 1
                three = True

    return twice * thrice


def problem_2(a):
    start_index = -1
    prev = ''
    simmilar = 0
    result = []
    while start_index <= (len(a) - 1):
        for i in a:
            prev = a[start_index]
            if not prev == i:
                for item in range(0, (len(i))):
                    if prev[item] == i[item]:
                        simmilar += 1
                        if simmilar == (len(i) - 1):
                            result.append(i.replace('\n', ''))
                            # Create new function that gives back
                            # correct result for the puzzle
                            # V(Uncomment print func to see right result)V
                            # print(result)
                            if len(result) > 1:
                                return result_problem_2(result)

                simmilar = 0

        start_index += 1


def result_problem_2(a):
    one = a[0]
    two = a[1]
    result = ''
    diff = list(Differ().compare(one, two))
    for i in diff:
        if i[0] == ' ':
            result = result + i.replace(' ', '')

    return result



if __name__ == '__main__':  # pragma: no cover
    puzzle_input_list = []
    with puzzle_input.open(mode='r', encoding='utf-8') as file:
        for i in file.readlines():  # pragma: no cover
            puzzle_input_list.append(i)

        problem_1(puzzle_input_list)  # This worked
        problem_2(puzzle_input_list)  # This worked
